package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeleteWorkspaceRequest {
    @NotNull
    private String code;
}
