package com.dustinhart.asu.api.clients.users;

import lombok.Data;

@Data
public class JwtValidateRequest {
    private String jwt;

    public JwtValidateRequest(String jwt) {
        this.jwt = jwt;
    }
}
