package com.dustinhart.asu.api.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "join_requests")
public class JoinRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToOne
    @JoinColumn(name = "workspace_id", referencedColumnName = "id")
    private Workspace workspace;
}
