package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.models.Report;
import com.dustinhart.asu.api.models.SummaryReport;
import com.dustinhart.asu.api.models.Team;
import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.repos.ReportRepo;
import com.dustinhart.asu.api.repos.SummaryReportRepo;
import com.dustinhart.asu.api.services.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ReportServiceImpl implements ReportService {

    private final ReportRepo reportRepo;
    private final SummaryReportRepo summaryRepo;
    private final UserService userService;
    private final EmailService emailService;
    private final TeamService teamService;
    private final WorkspaceService workspaceService;

    @Override
    public Report getReport(Long id) {
        return reportRepo.findById(id).orElse(null);
    }

    @Override
    public List<Report> getReports(String userEmail) {
        User user = userService.getUser(userEmail);
        if(user == null) {
            return Collections.emptyList();
        }

        return reportRepo.findByUserId(user.getId());
    }

    @Override
    public List<Report> getReports(String email, Long workspaceId) {
        return this.getReports(email).stream()
                .filter(r -> r.getWorkspace().getId().equals(workspaceId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Report> getReports(Long userId, Long workspaceId) {
        User user = userService.getUser(userId);
        if(user == null) return Collections.emptyList();

        return getReports(user.getEmail(), workspaceId);
    }

    @Override
    public List<Report> getReportsBetween(String userEmail, Long workspaceId, String startDate, String endDate) {
        User user = userService.getUser(userEmail);
        if(user == null) {
            return Collections.emptyList();
        }

        return reportRepo.findReportsBetween(user.getId(), workspaceId, startDate, endDate);
    }

    @Override
    public Report createReport(Report report) {
        report.setId(null);
        return reportRepo.save(report);
    }

    @Override
    public Report updateReport(User user, Report report) {
        Optional<Report> existingReport = reportRepo.findById(report.getId());
        if(existingReport.isEmpty()) {
            throw new RuntimeException("Report with id " + report.getId() + " does not exist");
        }
        User expectedUser = existingReport.get().getUser();
        if(expectedUser == null || expectedUser.getId() != user.getId()) {
            throw new RuntimeException("Report with id " + report.getId() + " does not belong to user " + user.getEmail());
        }

        // don't allow the workspace to be updated
        report.setWorkspace(existingReport.get().getWorkspace());
        report.setUser(user);
        return reportRepo.save(report);
    }

    @Override
    public SummaryReport getSummaryReport(Long id) {
        return summaryRepo.findById(id).orElse(null);
    }

    @Override
    public List<SummaryReport> getSummaryReports(String userEmail, Long workspaceId) {
        return this.getSummaryReports(userEmail).stream()
                .filter(r -> r.getWorkspace().getId().equals(workspaceId))
                .collect(Collectors.toList());
    }

    @Override
    public List<SummaryReport> getSummaryReports(String userEmail) {
        User user = userService.getUser(userEmail);
        if(user == null) {
            return Collections.emptyList();
        }

        return summaryRepo.findByUserId(user.getId());
    }

    @Override
    public List<SummaryReport> getSummaryReports(Long userId, Long workspaceId) {
        User user = userService.getUser(userId);
        if(user == null) return Collections.emptyList();

        return getSummaryReports(user.getEmail(), workspaceId);
    }

    @Override
    public SummaryReport createSummaryReport(SummaryReport report) {
        report.setId(null);
        summaryRepo.save(report);

        User user = report.getUser();
        List<Team> teams = teamService.getTeamsForUser(user.getId());

        for (Team t : teams) {
            emailService.sendSummaryReportNotification(t, user.getEmail(), report);
        }

        return report;
    }

    @Override
    public SummaryReport updateSummaryReport(User user, SummaryReport report) {
        Optional<SummaryReport> existingReport = summaryRepo.findById(report.getId());
        if(existingReport.isEmpty()) {
            throw new RuntimeException("Report with id " + report.getId() + " does not exist");
        }
        User expectedUser = existingReport.get().getUser();
        if(expectedUser == null || expectedUser.getId() != user.getId()) {
            throw new RuntimeException("Report with id " + report.getId() + " does not belong to user " + user.getEmail());
        }

        // don't allow the workspace to be updated
        report.setWorkspace(existingReport.get().getWorkspace());
        report.setUser(user);

        return summaryRepo.save(report);
    }


    @Override
    public boolean canSeeReportsForUser(User accessingUser, Long workspaceId, Long userId) {
        // user can see their own reports
        if(accessingUser.getId().equals(userId)) return true;

        // workspace owner can see all reports
        return workspaceService.isWorkspaceOwner(accessingUser.getId(), workspaceId);
    }

    @Override
    public boolean canSeeSummariesForUser(User accessingUser, Long workspaceId, Long userId) {
        // user can see their own reports
        if(accessingUser.getId().equals(userId)) return true;

        // workspace owner can see all reports
        if(workspaceService.isWorkspaceOwner(accessingUser.getId(), workspaceId)) return true;

        // manager of owner's teams can see summary reports
        return teamService.isManager(accessingUser.getId(), userId);
    }
}
