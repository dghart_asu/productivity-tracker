package com.dustinhart.asu.api.resources.v1.dtos;

import com.dustinhart.asu.api.models.Permission;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PermissionDto {
    // workspaces that the user belongs to
    private List<Long> workspaces;

    // workspaces that the user is an owner of
    private List<Long> owner;

    // teams that a user is a (non-manager) member of
    private List<Long> teams;

    // teams that the user is a manager of
    private List<Long> manager;

    public PermissionDto(List<Permission> permissions) {
        this.workspaces = new ArrayList<>();
        this.owner = new ArrayList<>();
        this.teams = new ArrayList<>();
        this.manager = new ArrayList<>();

        for (Permission p : permissions) {
            switch (p.getType()) {
                case WORKSPACE_OWNER:
                    owner.add(p.getResourceId());
                    break;
                case WORKSPACE_MEMBER:
                    workspaces.add(p.getResourceId());
                    break;
                case TEAM_MANAGER:
                    manager.add(p.getResourceId());
                    break;
                case TEAM_MEMBER:
                    teams.add(p.getResourceId());
                    break;
            }
        }
    }
}
