package com.dustinhart.asu.api.exceptions;

public class WorkspaceNotFoundException extends WorkspaceException {
    public WorkspaceNotFoundException(String message) {
        super(message, 404);
    }
}
