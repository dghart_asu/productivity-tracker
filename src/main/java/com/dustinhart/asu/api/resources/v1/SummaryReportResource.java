package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.ReportNotFoundException;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.models.Report;
import com.dustinhart.asu.api.models.SummaryReport;
import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.repos.ReportRepo;
import com.dustinhart.asu.api.repos.SummaryReportRepo;
import com.dustinhart.asu.api.resources.v1.dtos.CreateReportRequest;
import com.dustinhart.asu.api.resources.v1.dtos.CreateSummaryReportRequest;
import com.dustinhart.asu.api.resources.v1.dtos.ErrorResponse;
import com.dustinhart.asu.api.services.ReportService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ws/v1/summaries")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class SummaryReportResource {

    private final WorkspaceService workspaceService;
    private final ReportService reportService;
    private final LoggedInUserService auth;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getReport(@PathVariable("id") String id) {
        SummaryReport report = reportService.getSummaryReport(Long.parseLong(id));
        return ResponseEntity.ok(report);
    }

    @RequestMapping(value = "/preview", method = RequestMethod.GET)
    public ResponseEntity<?> getReportsPreview(
            @RequestParam(value = "workspace") Long workspaceId,
            @RequestParam(value = "startDate") String startDate,
            @RequestParam(value = "endDate") String endDate
    ) {
        User user = auth.getPrincipalUser();

        if (!workspaceService.belongsToWorkspace(user.getId(), workspaceId)) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }

        List<Report> reports = reportService.getReportsBetween(
                user.getEmail(),
                workspaceId,
                startDate,
                endDate);

        return ResponseEntity.ok(reports);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getReports(@RequestParam(value = "workspace", required = false) Long workspaceId) {
        User user = auth.getPrincipalUser();
        Long userId = user.getId();
        String email = user.getEmail();

        List<SummaryReport> reports;
        if (workspaceId != null) {
            if (!workspaceService.belongsToWorkspace(userId, workspaceId)) {
                throw new WorkspaceException("User does not belong to this workspace", 401);
            }
            reports = reportService.getSummaryReports(email, workspaceId);
        } else {
            reports = reportService.getSummaryReports(email);
        }

        return ResponseEntity.ok(reports);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createSummaryReport(@RequestBody CreateSummaryReportRequest request) {
        ModelMapper modelMapper = new ModelMapper();

        SummaryReport report = modelMapper.map(request, SummaryReport.class);
        report.setId(null);
        report.setUser(auth.getPrincipalUser());

        if (!workspaceService.belongsToWorkspace(report.getUser().getId(), request.getWorkspaceId())) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }

        List<Report> reports = reportService.getReportsBetween(
                auth.getPrincipalUser().getEmail(),
                request.getWorkspaceId(),
                request.getStartDate(),
                request.getEndDate());

        report.setReports(reports);

        return ResponseEntity.ok(reportService.createSummaryReport(report));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateSummaryReport(@RequestBody CreateSummaryReportRequest request) {
        User user = auth.getPrincipalUser();

        ModelMapper modelMapper = new ModelMapper();
        SummaryReport report = modelMapper.map(request, SummaryReport.class);

        SummaryReport existingReport = reportService.getSummaryReport(report.getId());
        if(existingReport.getUser().getId() != user.getId()) {
            throw new ReportNotFoundException("The report with id " + report.getId() + " does not belong to this user");
        }
        report.setUser(user);

        List<Report> reports = reportService.getReportsBetween(
                user.getEmail(),
                existingReport.getWorkspace().getId(),
                request.getStartDate(),
                request.getEndDate());

        report.setReports(reports);

        return ResponseEntity.ok(reportService.updateSummaryReport(user, report));
    }

    @ExceptionHandler(WorkspaceException.class)
    public ResponseEntity<?> handlException(WorkspaceException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }
}
