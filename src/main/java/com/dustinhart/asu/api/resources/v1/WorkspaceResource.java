package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.exceptions.WorkspaceNotFoundException;
import com.dustinhart.asu.api.models.Workspace;
import com.dustinhart.asu.api.resources.v1.dtos.CreateWorkspaceRequest;
import com.dustinhart.asu.api.resources.v1.dtos.DeleteWorkspaceRequest;
import com.dustinhart.asu.api.resources.v1.dtos.ErrorResponse;
import com.dustinhart.asu.api.security.UserPrincipal;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ws/v1/workspaces")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class WorkspaceResource {

    private final WorkspaceService workspaceService;
    private final LoggedInUserService auth;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getWorkspaces() {
        List<Workspace> workspaces = workspaceService.getWorkspaces(auth.getPrincipal().getEmail(), true);
        return ResponseEntity.ok(workspaces);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createWorkspace(@Valid @RequestBody CreateWorkspaceRequest request) {
        String userId = auth.getPrincipal().getEmail();

        if(!request.getCode().matches("^[A-Za-z0-9\\-_]+$")) {
            throw new WorkspaceException("Workspace codes can only contain letters, numbers, - and _ symbols.", 409);
        }

        Workspace workspace = workspaceService.createWorkspace(userId, request.getName(), request.getCode());
        return ResponseEntity.ok(workspace);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteWorkspace(@PathVariable("id") long id) {
        UserPrincipal principal = auth.getPrincipal();
        if(principal == null) {
            throw new WorkspaceNotFoundException("The user has no workspaces");
        }

        workspaceService.deleteWorkspace(principal.getEmail(), id);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(WorkspaceException.class)
    public ResponseEntity<?> handlException(WorkspaceException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }
}
