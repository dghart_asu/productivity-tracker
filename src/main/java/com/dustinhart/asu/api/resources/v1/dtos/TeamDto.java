package com.dustinhart.asu.api.resources.v1.dtos;

import com.dustinhart.asu.api.models.Team;
import com.dustinhart.asu.api.models.TeamMember;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class TeamDto {
    private Long id;
    private String name;
    private List<TeamMemberDto> members;

    public TeamDto(Team t, List<TeamMember> members) {
        this.id = t.getId();
        this.name = t.getName();

        if(members != null) {
            this.members = members.stream()
                    .map( tm -> new TeamMemberDto(tm))
                    .collect(Collectors.toList());
        }
    }

    @Data
    public static class TeamMemberDto {
        private Long membershipId;
        private Long userId;
        private String email;
        private String role;

        public TeamMemberDto(TeamMember tm) {
            this.membershipId = tm.getTeamMembershipId();
            this.userId = tm.getUserId();
            this.email = tm.getEmail();
            this.role = tm.getRole();
        }
    }
}
