#!/bin/sh 

./mvnw clean package -DskipTests
mkdir build_tmp
cp target/tracker-api-*.jar build_tmp/
cp -r .ebextensions build_tmp/
cd build_tmp
zip -r ../deployment.zip .
rm -rf build_tmp
