package com.dustinhart.asu.api.clients;

import com.dustinhart.asu.api.clients.users.ChangePasswordRequest;
import com.dustinhart.asu.api.clients.users.JwtValidateRequest;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@Headers("Content-Type: application/json")
public interface UserServiceClient {

    @RequestLine("POST /validate")
    void validate(JwtValidateRequest req);

    @RequestLine("POST /changePassword")
    @Headers("Authorization: {auth_header}")
    void changePassword(@Param("auth_header") String authHeader, ChangePasswordRequest req);

    static String createAuthHeader(String jwt) {
        return "Bearer " + jwt;
    }
}
