package com.dustinhart.asu.api.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.jsonwebtoken.Claims;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class UserPrincipal implements UserDetails {
    private String id;

    private String name;

    private String username;

    private String email;

    private String password;

    private String jwt;

    private Collection<? extends GrantedAuthority> authorities;

    public static UserPrincipal create(String jwt, Claims claims) {
        List<GrantedAuthority> authorities = (List<GrantedAuthority>) claims.get("authorities", List.class)
                .stream()
                .map(a -> new SimpleGrantedAuthority((String) a))
                .collect(Collectors.toList());

        UserPrincipal p = new UserPrincipal();
        p.setId(claims.getId());
        p.setEmail(claims.get("email", String.class));
        p.setUsername(p.getEmail());
        p.setName(claims.get("name", String.class));
        p.setAuthorities(authorities);
        p.setJwt(jwt);

        return p;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
