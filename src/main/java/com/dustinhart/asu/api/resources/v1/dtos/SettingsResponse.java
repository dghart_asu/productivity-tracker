package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

@Data
public class SettingsResponse {
    private boolean notifications;

    public SettingsResponse(boolean enableNotifications) {
        this.notifications = enableNotifications;
    }
}
