package com.dustinhart.asu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "reports")
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    @OneToOne
    @JoinColumn(name = "workspace_id", referencedColumnName = "id")
    @JsonIgnore
    private Workspace workspace;

    private String date;
    private String yesterday;
    private String today;
    private String blockers;
    private String notes;
    private String feedback;
}
