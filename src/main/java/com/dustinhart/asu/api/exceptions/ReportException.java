package com.dustinhart.asu.api.exceptions;

public class ReportException extends RuntimeException {
    public ReportException(String message) {
        super(message);
    }
}
