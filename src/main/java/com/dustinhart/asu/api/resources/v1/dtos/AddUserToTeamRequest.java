package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

@Data
public class AddUserToTeamRequest {
    private Long user;
    private String role;
}
