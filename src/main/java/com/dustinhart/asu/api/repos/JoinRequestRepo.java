package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.JoinRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JoinRequestRepo extends JpaRepository<JoinRequest, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM join_requests WHERE workspace_id IN (:workspaceIds)")
    List<JoinRequest> findByWorkspaceIdIn(@Param("workspaceIds") List<Long> workspaceIds);
    List<JoinRequest> findByUserId(Long userId);
}
