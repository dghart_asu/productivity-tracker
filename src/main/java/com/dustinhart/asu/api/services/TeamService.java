package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.Team;
import com.dustinhart.asu.api.models.TeamMember;

import java.util.List;

public interface TeamService {
    Team getTeam(Long teamId);
    List<Team> getTeamsForUser(Long userId);
    List<Team> getTeamsForWorkspace(Long workspaceId);
    Team createTeam(Long workspaceId, String name);
    void deleteTeam(Long ownerId, Long teamId);
    void addUserToTeam(Long teamId, Long userId, String role);
    void removeUserFromTeam(Long teamId, Long userId);
    List<TeamMember> getUsersForTeam(Long teamId);
    boolean allowedToCreateTeams(Long userId, Long workspaceId);
    boolean isManager(Long managerId, Long userId);
}
