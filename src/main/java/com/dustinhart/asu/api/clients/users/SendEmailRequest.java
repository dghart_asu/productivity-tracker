package com.dustinhart.asu.api.clients.users;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class SendEmailRequest {
    private String targetEmail;
    private Map<String, Object> attributes;

    public SendEmailRequest(String targetEmail) {
        this.targetEmail = targetEmail;
        this.attributes = new HashMap<>();
    }

    public void addAttribute(String key, Object value) {
        attributes.put(key, value);
    }
}
