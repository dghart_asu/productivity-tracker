package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.clients.EmailServiceClient;
import com.dustinhart.asu.api.clients.UserServiceClient;
import com.dustinhart.asu.api.clients.users.SendEmailRequest;
import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.models.*;
import com.dustinhart.asu.api.repos.TeamMembershipRepo;
import com.dustinhart.asu.api.services.EmailService;
import com.dustinhart.asu.api.services.SettingsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {

    @Value("${app.notifications.inactiveDays}")
    private int inactiveDays;

    private final LoggedInUserService auth;
    private final SettingsService settingsService;
    private final EmailServiceClient emailServiceClient;
    private final TeamMembershipRepo teamMembershipRepo;

    @Override
    public void sendActivityReminder(String email, Long workspaceId) {
        Settings settings = settingsService.getSettings(email);
        if (!settings.isEnableNotifications()) {
            log.debug("Email notifications are not enabled for user {}", email);
            return;
        }

        String jwt = auth.getJwt();
        SendEmailRequest request = new SendEmailRequest(email);
        request.addAttribute("inactiveDays", inactiveDays);
        request.addAttribute("workspaceId", workspaceId);

        log.debug("Sending activity reminder {}", email);
        emailServiceClient.activityReminder(UserServiceClient.createAuthHeader(jwt), request);
    }

    @Override
    public void sendSummaryReportNotification(Team team, String userEmail, SummaryReport report) {
        User manager = findManager(team);
        if (manager == null) {
            log.debug("There is no manager for the team {} to email about summary report for ({}, {})",
                    team.getName(), userEmail, report.getId());
            return;
        }

        Settings settings = settingsService.getSettings(manager.getEmail());
        if (!settings.isEnableNotifications()) {
            log.debug("Email notifications are not enabled for user {}", manager.getEmail());
            return;
        }

        String jwt = auth.getJwt();
        SendEmailRequest request = new SendEmailRequest(manager.getEmail());
        request.addAttribute("posterName", userEmail);
        request.addAttribute("teamName", team.getName());
        request.addAttribute("workspaceId", report.getWorkspace().getId());
        request.addAttribute("reportId", report.getId());

        log.debug("Sending summary report notification to manager {}", manager.getEmail());
        emailServiceClient.summaryReport(UserServiceClient.createAuthHeader(jwt), request);
    }

    private User findManager(Team team) {
        for (TeamMembership m : teamMembershipRepo.findByTeamId(team.getId())) {
            if ("MANAGER".equals(m.getRole())) {
                return m.getUser();
            }
        }
        return null;
    }
}
