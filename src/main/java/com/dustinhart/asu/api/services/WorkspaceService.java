package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.JoinRequest;
import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.models.Workspace;

import java.util.List;

public interface WorkspaceService {
    Workspace getWorkspace(Long workspaceId);
    List<Workspace> getWorkspaces(Long userId);
    List<Workspace> getWorkspaces(String userEmail);
    List<Workspace> getWorkspaces(String userEmail, boolean includePending);
    Workspace createWorkspace(String userEmail, String name, String code);
    void deleteWorkspace(String userEmail, Long workspaceId);
    void leaveWorkspace(String userEmail, Long workspaceId);
    void removeFromWorkspace(User owner, Long workspaceId, Long userId);
    List<JoinRequest> getJoinRequests(Long userId);
    void requestJoinWorkspace(String userEmail, String workspaceCode);
    void answerJoinRequest(Long userId, Long joinRequestId, boolean answer);
    boolean belongsToWorkspace(Long userId, Long workspaceId);
    boolean isWorkspaceOwner(Long userId, Long workspaceId);
}
