package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateReportRequest {
    private Long id;
    @NotBlank
    private Long workspaceId;
    @NotBlank
    private String date;
    private String yesterday;
    private String today;
    private String blockers;
    private String notes;
    private String feedback;

    public boolean isEmpty() {
        return isEmpty(yesterday) &&
                isEmpty(today) &&
                isEmpty(blockers) &&
                isEmpty(notes) &&
                isEmpty(feedback);
    }

    private boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }
}
