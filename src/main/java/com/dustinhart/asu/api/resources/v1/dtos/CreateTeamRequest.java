package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateTeamRequest {
    @NotNull
    private Long workspace;

    @NotBlank
    private String name;
}
