package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.repos.MembershipRepo;
import com.dustinhart.asu.api.repos.UserRepo;
import com.dustinhart.asu.api.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final MembershipRepo membershipRepo;

    @Override
    public User getUser(Long userId) {
        return userRepo.findById(userId).orElse(null);
    }

    @Override
    public User getUser(String email) {
        return userRepo.findByEmail(email);
    }

    @Override
    public User getOrCreateUser(String email) {
        email = email.toLowerCase();

        User user = userRepo.findByEmail(email);
        if(user != null) {
            return user;
        }

        user = new User();
        user.setEmail(email);

        return userRepo.save(user);
    }

    @Override
    public List<User> getUsers(Long workspaceId) {
        return membershipRepo.findByWorkspaceId(workspaceId).stream()
                .map( m -> m.getUser()).collect(Collectors.toList());
    }
}
