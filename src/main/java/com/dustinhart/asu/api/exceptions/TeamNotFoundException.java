package com.dustinhart.asu.api.exceptions;

import lombok.Data;

@Data
public class TeamNotFoundException extends RuntimeException {
    private final int status;

    public TeamNotFoundException(String message) {
        super(message);
        this.status = 404;
    }

    public int getStatus() {
        return status;
    }
}
