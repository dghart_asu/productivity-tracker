package com.dustinhart.asu.api.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "team_memberships")
public class TeamMembership {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToOne
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    private Team team;

    private String role;
}
