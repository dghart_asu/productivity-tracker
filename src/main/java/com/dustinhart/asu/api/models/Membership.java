package com.dustinhart.asu.api.models;

import lombok.Data;

import javax.persistence.*;

// https://www.baeldung.com/jpa-one-to-one
@Data
@Entity(name = "memberships")
public class Membership {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToOne
    @JoinColumn(name = "workspace_id", referencedColumnName = "id")
    private Workspace workspace;

    private String role;
}
