package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.Settings;

public interface SettingsService {
    Settings getSettings(String userEmail);
    Settings updateSettings(String userEmail, boolean enableNotifications, String oldPassword, String newPassword);
}
