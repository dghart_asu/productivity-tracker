package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.TeamMembership;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamMembershipRepo extends JpaRepository<TeamMembership, Long> {
    List<TeamMembership> findByUserId(Long userId);
    List<TeamMembership> findByTeamId(Long teamId);
    TeamMembership findByUserIdAndTeamId(Long userId, Long teamId);
}
