package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

@Data
public class UpdateSettingsRequest {
    private boolean notifications;
    private String oldPassword;
    private String newPassword;
}
