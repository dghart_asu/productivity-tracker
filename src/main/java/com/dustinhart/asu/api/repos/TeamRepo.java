package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepo extends JpaRepository<Team, Long> {
    List<Team> findByWorkspaceId(Long workspaceId);
}
