package com.dustinhart.asu.api.resources.v1.dtos;

import com.dustinhart.asu.api.models.User;
import lombok.Data;

@Data
public class UserDto {
    private Long id;
    private String email;
    private PermissionDto permissions;

    public UserDto(User user, PermissionDto permissions) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.permissions = permissions;
    }
}
