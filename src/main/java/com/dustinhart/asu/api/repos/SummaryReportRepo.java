package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.SummaryReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SummaryReportRepo extends JpaRepository<SummaryReport, Long> {
    List<SummaryReport> findByUserId(Long id);
}
