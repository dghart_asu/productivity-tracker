package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepo extends JpaRepository<Report, Long> {
    List<Report> findByUserId(Long id);

    @Query(nativeQuery = true,
            value = "SELECT * " +
                    "FROM reports r " +
                    "WHERE " +
                      "r.user_id = ?1 AND " +
                      "r.workspace_id = ?2 AND " +
                      "TO_DATE(r.date, 'YYYY-MM-DD') " +
                      "between TO_DATE(?3, 'YYYY-MM-DD') and TO_DATE(?4, 'YYYY-MM-DD')"
    )
    List<Report> findReportsBetween(Long userId, Long workspaceId, String startDate, String endDate);
}
