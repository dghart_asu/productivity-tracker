package com.dustinhart.asu.api.clients;

import com.dustinhart.asu.api.clients.users.SendEmailRequest;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

@Headers("Content-Type: application/json")
public interface EmailServiceClient {

    @RequestLine("POST /activityReminder")
    @Headers("Authorization: {auth_header}")
    void activityReminder(@Param("auth_header") String authHeader, SendEmailRequest req);

    @RequestLine("POST /summaryReport")
    @Headers("Authorization: {auth_header}")
    void summaryReport(@Param("auth_header") String authHeader, SendEmailRequest req);

    static String createAuthHeader(String jwt) {
        return "Bearer " + jwt;
    }
}
