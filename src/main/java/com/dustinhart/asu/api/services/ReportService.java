package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.Report;
import com.dustinhart.asu.api.models.SummaryReport;
import com.dustinhart.asu.api.models.User;

import java.util.List;

public interface ReportService {
    Report getReport(Long id);
    List<Report> getReports(String email, Long workspaceId);
    List<Report> getReports(String userEmail);
    List<Report> getReports(Long userId, Long workspaceId);
    List<Report> getReportsBetween(String userEmail, Long workspaceId, String startDate, String endDate);
    Report createReport(Report report);
    Report updateReport(User user, Report report);
    boolean canSeeReportsForUser(User accessingUser, Long workspaceId, Long userId);

    SummaryReport getSummaryReport(Long id);
    List<SummaryReport> getSummaryReports(String userEmail, Long workspaceId);
    List<SummaryReport> getSummaryReports(String userEmail);
    List<SummaryReport> getSummaryReports(Long userId, Long workspaceId);
    SummaryReport createSummaryReport(SummaryReport report);
    SummaryReport updateSummaryReport(User user, SummaryReport report);
    boolean canSeeSummariesForUser(User accessingUser, Long workspaceId, Long userId);
}
