package com.dustinhart.asu.api.exceptions;

public class ReportNotFoundException extends ReportException {
    public ReportNotFoundException(String message) {
        super(message);
    }
}
