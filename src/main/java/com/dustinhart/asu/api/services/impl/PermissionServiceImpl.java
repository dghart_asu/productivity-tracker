package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.models.*;
import com.dustinhart.asu.api.repos.JoinRequestRepo;
import com.dustinhart.asu.api.repos.MembershipRepo;
import com.dustinhart.asu.api.repos.WorkspaceRepo;
import com.dustinhart.asu.api.services.PermissionService;
import com.dustinhart.asu.api.services.TeamService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PermissionServiceImpl implements PermissionService {

    private final WorkspaceService workspaceService;
    private final TeamService teamService;
    private final LoggedInUserService auth;

    @Override
    public List<Permission> getPermissions(Long userId) {

        List<Permission> ret = new ArrayList<>();

        List<Workspace> workspaces = workspaceService.getWorkspaces(userId);
        workspaces.stream().forEach( w -> {
            if(w.getStatus().equals("OWNER")) {
                ret.add(new Permission(Permission.PermissionType.WORKSPACE_OWNER, w.getId()));
            } else {
                ret.add(new Permission(Permission.PermissionType.WORKSPACE_MEMBER, w.getId()));
            }
        });

        for(Team team : teamService.getTeamsForUser(userId)) {
            List<TeamMember> teamMemberships = teamService.getUsersForTeam(team.getId());
            teamMemberships.stream().forEach( tm -> {
                if(!tm.getUserId().equals(userId)) return;

                if(tm.getRole().equals("MANAGER")) {
                    ret.add(new Permission(Permission.PermissionType.TEAM_MANAGER, tm.getTeamId()));
                } else {
                    ret.add(new Permission(Permission.PermissionType.TEAM_MEMBER, tm.getTeamId()));
                }
            });
        }

        return ret;
    }

    @Override
    public List<Permission> getPermissions(Long userId, Long workspaceId) {

        List<Permission> ret = new ArrayList<>();

        List<Workspace> workspaces = workspaceService.getWorkspaces(userId);
        workspaces.stream().forEach( w -> {
            if(!w.getId().equals(workspaceId)) return;

            if(w.getStatus().equals("OWNER")) {
                ret.add(new Permission(Permission.PermissionType.WORKSPACE_OWNER, w.getId()));
            } else {
                ret.add(new Permission(Permission.PermissionType.WORKSPACE_MEMBER, w.getId()));
            }
        });

        for(Team team : teamService.getTeamsForUser(userId)) {
            if(!workspaceId.equals(team.getWorkspace().getId())) continue;

            List<TeamMember> teamMemberships = teamService.getUsersForTeam(team.getId());
            teamMemberships.stream().forEach( tm -> {
                if(!tm.getUserId().equals(userId)) return;

                if(tm.getRole().equals("MANAGER")) {
                    ret.add(new Permission(Permission.PermissionType.TEAM_MANAGER, tm.getTeamId()));
                } else {
                    ret.add(new Permission(Permission.PermissionType.TEAM_MEMBER, tm.getTeamId()));
                }
            });
        }

        return ret;
    }
}
