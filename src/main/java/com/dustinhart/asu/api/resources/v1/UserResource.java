package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.UserNotFoundException;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.models.Report;
import com.dustinhart.asu.api.models.SummaryReport;
import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.resources.v1.dtos.ErrorResponse;
import com.dustinhart.asu.api.resources.v1.dtos.PermissionDto;
import com.dustinhart.asu.api.resources.v1.dtos.UserDto;
import com.dustinhart.asu.api.services.PermissionService;
import com.dustinhart.asu.api.services.ReportService;
import com.dustinhart.asu.api.services.UserService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ws/v1/workspaces/{workspace_id}/users")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class UserResource {
    private final UserService userService;
    private final PermissionService permissionService;
    private final WorkspaceService workspaceService;
    private final ReportService reportService;
    private final LoggedInUserService auth;

    @RequestMapping(value="/{user_id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUser(
            @PathVariable("workspace_id") Long workspaceId,
            @PathVariable("user_id") Long userId
    ) {
        checkPermissions(workspaceId);

        User user = userService.getUser(userId);
        if(user == null) {
            throw new UserNotFoundException("There is no user with the id " + userId);
        }

        return ResponseEntity.ok(new UserDto(user, new PermissionDto(permissionService.getPermissions(user.getId(), workspaceId))));
    }

    // Returns the reports for a given user
    // workspace owner can see all reports
    // manager of user's team can see summaries
    // other users can't see anything
    @RequestMapping(value="/{user_id}/reports", method = RequestMethod.GET)
    public ResponseEntity<?> getReportsForUser(
            @PathVariable("workspace_id") Long workspaceId,
            @PathVariable("user_id") Long userId
    ) {
        if(!reportService.canSeeReportsForUser(auth.getPrincipalUser(), workspaceId, userId)) {
            return ResponseEntity.ok(new ArrayList<>());
        }

        List<Report> reports = reportService.getReports(userId, workspaceId);
        return ResponseEntity.ok(reports);
    }

    @RequestMapping(value="/{user_id}/summaries", method = RequestMethod.GET)
    public ResponseEntity<?> getSummariesForUser(
            @PathVariable("workspace_id") Long workspaceId,
            @PathVariable("user_id") Long userId
    ) {
        if(!reportService.canSeeSummariesForUser(auth.getPrincipalUser(), workspaceId, userId)) {
            return ResponseEntity.ok(new ArrayList<>());
        }

        List<SummaryReport> reports = reportService.getSummaryReports(userId, workspaceId);
        return ResponseEntity.ok(reports);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getUsers(@PathVariable("workspace_id") Long workspaceId) {
        checkPermissions(workspaceId);

        List<UserDto> users = userService.getUsers(workspaceId).stream().map(u ->
            new UserDto(u, new PermissionDto(permissionService.getPermissions(u.getId(), workspaceId)))
        ).collect(Collectors.toList());

        return ResponseEntity.ok(users);
    }

    /**
     * Principal user (user making the request) must belong to the workspace.
     * @param workspaceId id for the workspace that the user is trying to access
     * @throws WorkspaceException if user does not have access
     */
    private void checkPermissions(Long workspaceId) {
        User user = auth.getPrincipalUser();
        if (!workspaceService.belongsToWorkspace(user.getId(), workspaceId)) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }
    }

    @ExceptionHandler(WorkspaceException.class)
    public ResponseEntity<?> handlException(WorkspaceException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<?> handlException(UserNotFoundException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(404).body(res);
    }
}