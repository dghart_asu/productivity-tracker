package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.exceptions.WorkspaceNotFoundException;
import com.dustinhart.asu.api.models.*;
import com.dustinhart.asu.api.repos.*;
import com.dustinhart.asu.api.services.UserService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class WorkspaceServiceImpl implements WorkspaceService {

    private final WorkspaceRepo workspaceRepo;
    private final MembershipRepo membershipRepo;
    private final JoinRequestRepo joinRequestRepo;
    private final TeamMembershipRepo teamMembershipRepo;
    private final TeamRepo teamRepo;
    private final ReportRepo reportRepo;
    private final SummaryReportRepo summaryRepo;
    private final UserService userService;

    @Override
    public Workspace getWorkspace(Long workspaceId) {
        return workspaceRepo.findById(workspaceId).orElse(null);
    }

    @Override
    public List<Workspace> getWorkspaces(Long userId) {
        List<Workspace> ret = new ArrayList<>();

        // Get users's existing workspaces
        membershipRepo.findByUserId(userId).stream().forEach(m -> {
            Workspace w = m.getWorkspace();
            w.setStatus(m.getRole());
            ret.add(w);
        });

        return ret;
    }

    @Override
    public List<Workspace> getWorkspaces(String userEmail) {
        return getWorkspaces(userEmail, false);
    }

    @Override
    public List<Workspace> getWorkspaces(String userEmail, boolean includePending) {
        User user = userService.getUser(userEmail);
        if (user == null) {
            return Collections.emptyList();
        }

        List<Workspace> ret = new ArrayList<>();

        // Get users's existing workspaces
        membershipRepo.findByUserId(user.getId()).stream().forEach(m -> {
            Workspace w = m.getWorkspace();
            w.setStatus(m.getRole());
            ret.add(w);
        });

        if (includePending) {
            // Get pending workspaces
            for (JoinRequest jr : joinRequestRepo.findByUserId(user.getId())) {
                Workspace w = jr.getWorkspace();
                w.setStatus("PENDING");
                ret.add(w);
            }
        }


        return ret;
    }

    @Override
    @Transactional
    public Workspace createWorkspace(String userEmail, String name, String code) {
        User user = userService.getOrCreateUser(userEmail);

        Workspace workspace = new Workspace();
        workspace.setName(name);
        workspace.setCode(code.toLowerCase());

        workspaceRepo.save(workspace);

        Membership membership = new Membership();
        membership.setUser(user);
        membership.setWorkspace(workspace);
        membership.setRole("OWNER");

        membershipRepo.save(membership);

        log.info("Created workspace with code {} for the user {}", code, user.getEmail());

        workspace.setStatus("OWNER");

        return workspace;
    }

    @Override
    @Transactional
    public void deleteWorkspace(String userEmail, Long workspaceId) {
        User user = userService.getUser(userEmail);
        if (user == null) {
            return;
        }

        Workspace workspace = workspaceRepo.findById(workspaceId).orElse(null);
        if (workspace == null) {
            throw new WorkspaceNotFoundException("A workspace does not exist with the id " + workspaceId);
        }

        Membership membership = membershipRepo.findByUserIdAndWorkspaceId(user.getId(), workspace.getId());
        if (membership == null) {
            throw new WorkspaceNotFoundException("The user " + userEmail + " does not belong to the workspace " + workspaceId);
        } else if (!membership.getRole().equals("OWNER")) {
            throw new WorkspaceNotFoundException("The user " + userEmail + " is not an owner of the workspace " + workspaceId);
        }

        // Remove users
        List<Membership> workspaceMembers = membershipRepo.findByWorkspaceId(workspaceId);
        for(Membership m : workspaceMembers) {
            removeUserFromWorkspace(workspaceId, m.getUser().getId());
        }

        // Remove join requests
        joinRequestRepo.deleteAll(joinRequestRepo.findByWorkspaceIdIn(Collections.singletonList(workspaceId)));

        // Remove teams
        teamRepo.deleteAll(teamRepo.findByWorkspaceId(workspaceId));

        workspaceRepo.delete(workspace);

        log.info("The user " + userEmail + " has deleted the workspace " + workspaceId);
    }

    @Override
    @Transactional
    public void leaveWorkspace(String userEmail, Long workspaceId) {
        User user = userService.getUser(userEmail);
        if (user == null) {
            return;
        }

        Workspace workspace = workspaceRepo.findById(workspaceId).orElse(null);
        if (workspace == null) {
            throw new WorkspaceNotFoundException("A workspace does not exist with the id " + workspaceId);
        }

        Membership membership = membershipRepo.findByUserIdAndWorkspaceId(user.getId(), workspace.getId());
        if (membership == null) {
            throw new WorkspaceNotFoundException("The user " + userEmail + " does not belong to the workspace " + workspaceId);
        }

        if (
                membership.getRole().equals("OWNER") &&
                        membershipRepo.countByWorkspaceIdAndRole(workspace.getId(), "OWNER") == 1
        ) {
            throw new WorkspaceException("You cannot leave a workspace if you are the last owner", 409);
        }

        removeUserFromWorkspace(workspaceId, user.getId());

        log.info("The user " + userEmail + " has left the workspace " + workspaceId);
    }

    @Override
    @Transactional
    public void removeFromWorkspace(User owner, Long workspaceId, Long userId) {
        if(owner.getId().equals(userId)) {
            throw new WorkspaceException("You cannot remove yourself from a workspace", 400);
        }
        if(!isWorkspaceOwner(owner.getId(), workspaceId)) {
            throw new WorkspaceException("User does not have permission to remove users from this workspace", 401);
        }

        removeUserFromWorkspace(workspaceId, userId);
    }

    @Override
    public void requestJoinWorkspace(String userEmail, String workspaceCode) {
        User user = userService.getOrCreateUser(userEmail);

        workspaceCode = workspaceCode.toLowerCase();
        Workspace workspace = workspaceRepo.findByCode(workspaceCode);
        if (workspace == null) {
            throw new WorkspaceNotFoundException("A workspace does not exist with the code " + workspaceCode);
        }

        Membership membership = membershipRepo.findByUserIdAndWorkspaceId(user.getId(), workspace.getId());
        if (membership != null) {
            String errorMessage = "The user " + userEmail + " already belongs to the workspace " + workspaceCode;
            throw new WorkspaceException(errorMessage, 409);
        }

        List<JoinRequest> currentRequests = joinRequestRepo.findByUserId(user.getId());
        for(JoinRequest j : currentRequests) {
            if(j.getWorkspace().getCode().equals(workspaceCode)) {
                // user already has a join request
                return;
            }
        }

        // create pending join
        JoinRequest req = new JoinRequest();
        req.setUser(user);
        req.setWorkspace(workspace);

        joinRequestRepo.save(req);
    }

    @Override
    @Transactional
    public void answerJoinRequest(Long userId, Long joinRequestId, boolean answer) {
        User principal = userService.getUser(userId);

        Optional<JoinRequest> results = joinRequestRepo.findById(joinRequestId);
        if(results.isEmpty()) {
            throw new WorkspaceException("This is no join request with the id " + joinRequestId, 404);
        }

        JoinRequest joinRequest = results.get();
        Membership membership = membershipRepo.findByUserIdAndWorkspaceId(principal.getId(), joinRequest.getWorkspace().getId());
        if(membership == null || !membership.getRole().equals("OWNER")) {
            String errorMessage = "The user " + principal.getEmail() +
                    " does not belong to the workspace or does not have permission to add users";
            throw new WorkspaceException(errorMessage, 409);
        }

        if(answer) {
            Membership newMembership = new Membership();
            newMembership.setRole("USER");
            newMembership.setUser(joinRequest.getUser());
            newMembership.setWorkspace(joinRequest.getWorkspace());
            membershipRepo.save(newMembership);
            joinRequestRepo.delete(joinRequest);
        } else {
            joinRequestRepo.delete(joinRequest);
            // Should we tell the user that the request was denied?
        }
    }

    @Override
    public List<JoinRequest> getJoinRequests(Long userId) {
        List<Membership> workspacesAsOwner = membershipRepo.findByUserIdAndRole(userId, "OWNER");
        List<Long> workspaceIds = workspacesAsOwner.stream().map(w -> w.getWorkspace().getId()).collect(Collectors.toList());
        return joinRequestRepo.findByWorkspaceIdIn(workspaceIds);
    }

    @Override
    public boolean belongsToWorkspace(Long userId, Long workspaceId) {
        try {
            return membershipRepo.findByUserIdAndWorkspaceId(userId, workspaceId) != null;
        } catch(EntityNotFoundException e) {
            return false;
        }
    }

    @Override
    public boolean isWorkspaceOwner(Long userId, Long workspaceId) {
        try {
            Membership membership = membershipRepo.findByUserIdAndWorkspaceId(userId, workspaceId);
            return membership != null && membership.getRole().equals("OWNER");
        } catch(EntityNotFoundException e) {
            return false;
        }
    }

    @Transactional
    private void removeUserFromWorkspace(Long workspaceId, Long userId) {
        Membership membership = membershipRepo.findByUserIdAndWorkspaceId(userId, workspaceId);
        if(membership == null) {
            throw new WorkspaceException("The user does not belong to this workspace", 400);
        }

        // remove remove summaries
        List<SummaryReport> summaryReports = summaryRepo.findByUserId(userId);
        summaryRepo.deleteAll(summaryReports.stream()
                .filter( s -> s.getWorkspace().getId().equals(workspaceId))
                .collect(Collectors.toList()));

        // remove reports
        List<Report> reports = reportRepo.findByUserId(userId);
        reportRepo.deleteAll(reports.stream()
                .filter( r -> r.getWorkspace().getId().equals(workspaceId))
                .collect(Collectors.toList()));

        // remove from teams
        List<TeamMembership> teamMembership = teamMembershipRepo.findByUserId(userId);
        teamMembershipRepo.deleteAll(teamMembership.stream()
                .filter( t-> t.getTeam().getWorkspace().getId().equals(workspaceId))
                .collect(Collectors.toList()));

        // Remove from workspace
        membershipRepo.delete(membership);
    }
}
