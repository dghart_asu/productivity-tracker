package com.dustinhart.asu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "teams")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "workspace_id", referencedColumnName = "id")
    @JsonIgnore
    private Workspace workspace;

    private String name;
}


