package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.User;

import java.util.List;

public interface UserService {
    User getUser(Long userId);
    User getUser(String email);
    User getOrCreateUser(String email);
    List<User> getUsers(Long workspaceId);
}
