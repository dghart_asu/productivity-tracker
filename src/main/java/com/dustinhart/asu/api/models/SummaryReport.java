package com.dustinhart.asu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "summary_reports")
public class SummaryReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    @OneToOne
    @JoinColumn(name = "workspace_id", referencedColumnName = "id")
    @JsonIgnore
    private Workspace workspace;

    private String date;

    private String publicNotes;
    private String privateNotes;

    @JoinTable(
            name = "summary_to_report",
            joinColumns = @JoinColumn(
                    name = "summary_id",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "report_id",
                    referencedColumnName = "id"
            )
    )
    @OneToMany(fetch = FetchType.EAGER)
    private List<Report> reports;
}
