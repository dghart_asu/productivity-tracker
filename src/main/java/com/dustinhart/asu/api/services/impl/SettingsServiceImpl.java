package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.clients.UserServiceClient;
import com.dustinhart.asu.api.clients.users.ChangePasswordRequest;
import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.InvalidPasswordException;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.exceptions.WorkspaceNotFoundException;
import com.dustinhart.asu.api.models.*;
import com.dustinhart.asu.api.repos.JoinRequestRepo;
import com.dustinhart.asu.api.repos.MembershipRepo;
import com.dustinhart.asu.api.repos.SettingsRepo;
import com.dustinhart.asu.api.repos.WorkspaceRepo;
import com.dustinhart.asu.api.security.UserPrincipal;
import com.dustinhart.asu.api.services.SettingsService;
import com.dustinhart.asu.api.services.UserService;
import com.dustinhart.asu.api.services.WorkspaceService;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class SettingsServiceImpl implements SettingsService {

    private final SettingsRepo settingsRepo;
    private final UserService userService;
    private final LoggedInUserService auth;
    private final UserServiceClient userServiceClient;

    @Override
    public Settings getSettings(String userEmail) {
        User user = userService.getOrCreateUser(userEmail);


        Settings settings = settingsRepo.findByUserId(user.getId());
        if(settings == null) {
            settings = new Settings();
            settings.setUser(user);
            settings.setEnableNotifications(true);
            settingsRepo.save(settings);
        }

        return settings;
    }

    @Override
    @Transactional
    public Settings updateSettings(
            String userEmail,
            boolean enableNotifications,
            String oldPassword,
            String newPassword
    ) {
        User user = userService.getOrCreateUser(userEmail);

        Settings settings = settingsRepo.findByUserId(user.getId());
        if(settings == null) {
            settings = new Settings();
            settings.setUser(user);

            settingsRepo.save(settings);
        }

        settings.setEnableNotifications(enableNotifications);

        if(
                (oldPassword == null && newPassword != null) ||
                (oldPassword != null && newPassword == null)
        ) {
            throw new InvalidPasswordException("The old or new password is blank. Please Try again.");
        }

        if(oldPassword != null && newPassword != null) {
            oldPassword = oldPassword.trim();
            newPassword = newPassword.trim();
            if(!isValidPassword(newPassword)) {
                throw new InvalidPasswordException("The new password must be at least 4 characters.");
            }

            String jwt = auth.getJwt();
            ChangePasswordRequest request = new ChangePasswordRequest(oldPassword, newPassword);
            try {
                userServiceClient.changePassword(UserServiceClient.createAuthHeader(jwt), request);
            } catch (FeignException e) {
                if(e.status() == 401) {
                    throw new InvalidPasswordException("The old or new password is not valid. Please Try again.");
                } else {
                    throw e;
                }
            }
        }

        settingsRepo.save(settings);

        return settings;
    }

    private boolean isValidPassword(String password) {
        password = password.trim();
        return password.length() >= 4;
    }
}
