package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.exceptions.WorkspaceNotFoundException;
import com.dustinhart.asu.api.models.JoinRequest;
import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.resources.v1.dtos.*;
import com.dustinhart.asu.api.security.UserPrincipal;
import com.dustinhart.asu.api.services.UserService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ws/v1/joinrequests")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class JoinRequestResource {
    private final WorkspaceService workspaceService;
    private final LoggedInUserService auth;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getRequestsToJoin() {
        User principal = auth.getPrincipalUser();

        List<JoinRequest> joinRequests = workspaceService.getJoinRequests(principal.getId());
        return ResponseEntity.ok(joinRequests);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> joinWorkspace(@Valid @RequestBody JoinWorkspaceRequest request) {
        UserPrincipal principal = auth.getPrincipal();

        workspaceService.requestJoinWorkspace(principal.getEmail(), request.getCode());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> answerJoinWorkspace(@Valid @RequestBody AnswerJoinWorkspaceRequest request) {
        workspaceService.answerJoinRequest(auth.getPrincipalUser().getId(), request.getId(), request.isAnswer());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity<?> removeFromWorkspace(
            @RequestParam(value = "workspace") Long workspaceId,
            @RequestParam(value = "user") Long userId
    ) {
        User principal = auth.getPrincipalUser();

        workspaceService.removeFromWorkspace(principal, workspaceId, userId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/{workspaceId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> leaveWorkspace(@PathVariable("workspaceId") long workspaceId) {
        UserPrincipal principal = auth.getPrincipal();
        if(principal == null) {
            throw new WorkspaceNotFoundException("The user has no workspaces");
        }

        workspaceService.leaveWorkspace(principal.getEmail(), workspaceId);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(WorkspaceException.class)
    public ResponseEntity<?> handlException(WorkspaceException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }
}
