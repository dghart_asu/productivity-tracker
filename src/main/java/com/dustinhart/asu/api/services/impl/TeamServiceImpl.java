package com.dustinhart.asu.api.services.impl;

import com.dustinhart.asu.api.exceptions.TeamNotFoundException;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.models.*;
import com.dustinhart.asu.api.repos.MembershipRepo;
import com.dustinhart.asu.api.repos.TeamMembershipRepo;
import com.dustinhart.asu.api.repos.TeamRepo;
import com.dustinhart.asu.api.repos.UserRepo;
import com.dustinhart.asu.api.services.TeamService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
@RequiredArgsConstructor
public class TeamServiceImpl implements TeamService {
    private final TeamRepo teamRepo;
    private final TeamMembershipRepo membershipRepo;
    private final WorkspaceService workspaceService;
    private final MembershipRepo workspaceMembershipRepo;
    private final UserRepo userRepo;

    @Override
    public Team getTeam(Long teamId) {
        return teamRepo.findById(teamId).orElse(null);
    }

    @Override
    public List<Team> getTeamsForUser(Long userId) {
        List<TeamMembership> memberships = membershipRepo.findByUserId(userId);
        return memberships.stream()
                .map(TeamMembership::getTeam)
                .collect(Collectors.toList());
    }

    @Override
    public List<Team> getTeamsForWorkspace(Long workspaceId) {
        return teamRepo.findByWorkspaceId(workspaceId);
    }

    @Override
    public Team createTeam(Long workspaceId, String name) {
        List<Team> existingTeams = teamRepo.findByWorkspaceId(workspaceId);

        boolean nameExists = existingTeams.stream()
                .anyMatch(t -> t.getName().equalsIgnoreCase(name));

        if(nameExists) {
            throw new TeamNotFoundException("There is already a team with that name");
        }

        Team team = new Team();
        team.setName(name);
        team.setWorkspace(workspaceService.getWorkspace(workspaceId));
        return teamRepo.save(team);
    }

    @Override
    @Transactional
    public void deleteTeam(Long ownerId, Long teamId) {
        Team team = getTeam(teamId);
        if(team == null) {
            throw new TeamNotFoundException("Team does not exist");
        }

        if(!allowedToCreateTeams(ownerId, team.getWorkspace().getId())) {
            throw new WorkspaceException("User is not allowed to delete teams", 401);
        }

        membershipRepo.deleteAll(membershipRepo.findByTeamId(team.getId()));
        teamRepo.delete(team);
    }

    @Override
    public List<TeamMember> getUsersForTeam(Long teamId) {
        List<TeamMembership> memberships = membershipRepo.findByTeamId(teamId);
        return memberships.stream()
                .map(m -> new TeamMember(m.getUser(), m))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void addUserToTeam(Long teamId, Long userId, String role) {
        // Check that team exists
        Team team = teamRepo.findById(teamId).orElse(null);
        if (team == null) {
            throw new TeamNotFoundException("This team does not exist");
        }

        // Check that user belongs to team's workspace
        if (!workspaceService.belongsToWorkspace(userId, team.getWorkspace().getId())) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }

        // User must exist if they belong to a workspace
        User user = userRepo.findById(userId).get();

        // Check if the user already belongs to the team
        TeamMembership membership = membershipRepo.findByUserIdAndTeamId(userId, teamId);
        if (membership == null) {
            // User does not belong to the team yet
            membership = new TeamMembership();
            membership.setUser(user);
            membership.setTeam(team);
            membership.setRole(role);
        } else {
            // User already belongs to the team, just change their role
            membership.setRole(role);
        }

        // If role is manager, change any other user with manager role to normal member
        if (role.equals("MANAGER")) {
            for (TeamMembership m : membershipRepo.findByTeamId(teamId)) {
                // If there is an existing manager who is not this user,
                if ("MANAGER".equals(m.getRole()) && m.getUser().getId() != userId) {
                    m.setRole("MEMBER");
                    membershipRepo.save(m);
                    break;
                }
            }
        }

        membershipRepo.save(membership);
    }

    @Override
    public void removeUserFromTeam(Long teamId, Long userId) {
        // Check that team exists
        Team team = teamRepo.findById(teamId).orElse(null);
        if (team == null) {
            throw new TeamNotFoundException("This team does not exist");
        }


        // No op if the user doesn't belong to team
        TeamMembership membership = membershipRepo.findByUserIdAndTeamId(userId, teamId);
        if (membership != null) {
            membershipRepo.delete(membership);
        }
    }

    @Override
    public boolean allowedToCreateTeams(Long userId, Long workspaceId) {
        Membership membership = workspaceMembershipRepo.findByUserIdAndWorkspaceId(userId, workspaceId);
        return membership != null && membership.getRole().equals("OWNER");
    }

    @Override
    public boolean isManager(Long managerId, Long userId) {
        List<Team> teams = getTeamsForUser(userId);
        return teams.stream().anyMatch( t -> {
            TeamMembership tm = membershipRepo.findByUserIdAndTeamId(managerId, t.getId());
            return tm != null && "MANAGER".equals(tm.getRole());
        });
    }
}
