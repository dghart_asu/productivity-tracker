package com.dustinhart.asu.api.models;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "settings")
public class Settings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    private boolean enableNotifications;
}
