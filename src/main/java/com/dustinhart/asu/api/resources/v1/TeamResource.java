package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.TeamNotFoundException;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.models.Team;
import com.dustinhart.asu.api.models.TeamMember;
import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.resources.v1.dtos.*;
import com.dustinhart.asu.api.services.TeamService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ws/v1/teams")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class TeamResource {
    private final TeamService teamService;
    private final WorkspaceService workspaceService;
    private final LoggedInUserService auth;

    @RequestMapping(path = "/{team_id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTeam(@PathVariable(value = "team_id") Long teamId) {
        Long userId = auth.getPrincipalUser().getId();


        Team team = teamService.getTeam(teamId);
        if(team == null) {
            throw new TeamNotFoundException("The team does not exist");
        }

        if (!workspaceService.belongsToWorkspace(userId, team.getWorkspace().getId())) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }

        TeamDto teamDto = new TeamDto(team, teamService.getUsersForTeam(team.getId()));
        return ResponseEntity.ok(teamDto);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getTeams(@RequestParam(value = "workspace", required = false) Long workspaceId) {
        Long userId = auth.getPrincipalUser().getId();

        List<Team> teams;
        if (workspaceId != null) {
            if (!workspaceService.belongsToWorkspace(userId, workspaceId)) {
                throw new WorkspaceException("User does not belong to this workspace", 401);
            }
            teams = teamService.getTeamsForWorkspace(workspaceId);
        } else {
            teams = teamService.getTeamsForUser(userId);
        }

        List<TeamDto> response = teams.stream()
                .map( t -> new TeamDto(t, teamService.getUsersForTeam(t.getId())))
                .collect(Collectors.toList());

        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createTeam(@Valid @RequestBody CreateTeamRequest request) {
        if (!teamService.allowedToCreateTeams(auth.getPrincipalUser().getId(), request.getWorkspace())) {
            throw new WorkspaceException("User does not have permission to create teams on this workspace", 401);
        }

        teamService.createTeam(request.getWorkspace(), request.getName());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/{team_id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTeam(@PathVariable("team_id") Long teamId) {
        teamService.deleteTeam(auth.getPrincipalUser().getId(), teamId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/{team_id}/users", method = RequestMethod.GET)
    public ResponseEntity<?> getUsers(@PathVariable("team_id") Long teamId) {
        Long userId = auth.getPrincipalUser().getId();

        Team team = teamService.getTeam(teamId);
        if(team == null) {
            throw new TeamNotFoundException("This team does not exist");
        } else if(!workspaceService.belongsToWorkspace(userId, team.getWorkspace().getId())) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }

        List<TeamMember> users = teamService.getUsersForTeam(teamId);
        // TODO get full user details from user service
        return ResponseEntity.ok(users);
    }

    @RequestMapping(path = "/{team_id}/users", method = RequestMethod.POST)
    public ResponseEntity<?> addUserToTeam(
            @PathVariable("team_id") Long teamId,
            @Valid @RequestBody AddUserToTeamRequest request
    ) {
        Long userId = auth.getPrincipalUser().getId();

        Team team = teamService.getTeam(teamId);
        if(team == null) {
            throw new TeamNotFoundException("This team does not exist");
        } else if(!workspaceService.isWorkspaceOwner(userId, team.getWorkspace().getId())) {
            throw new WorkspaceException("Only a workspace owner can add users to a team", 401);
        }

        teamService.addUserToTeam(teamId, request.getUser(), request.getRole());

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/{team_id}/users/{user_id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeUserFromTeam(
            @PathVariable("team_id") Long teamId,
            @PathVariable("user_id") Long userId
    ) {
        User principal = auth.getPrincipalUser();

        Team team = teamService.getTeam(teamId);
        if(team == null) {
            throw new TeamNotFoundException("This team does not exist");
        } else if(!workspaceService.isWorkspaceOwner(principal.getId(), team.getWorkspace().getId())) {
            throw new WorkspaceException("Only a workspace owner can remove users from a team", 401);
        }

        teamService.removeUserFromTeam(teamId, userId);

        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(WorkspaceException.class)
    public ResponseEntity<?> handlException(WorkspaceException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }

    @ExceptionHandler(TeamNotFoundException.class)
    public ResponseEntity<?> handlException(TeamNotFoundException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }
}
