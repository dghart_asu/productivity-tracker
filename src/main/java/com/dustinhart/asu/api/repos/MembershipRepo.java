package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.Membership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MembershipRepo extends JpaRepository<Membership, Long> {
    List<Membership> findByUserId(Long userId);
    Membership findByUserIdAndWorkspaceId(Long userId, Long workspaceId);
    List<Membership> findByWorkspaceId(Long workspaceId);
    List<Membership> findByUserIdAndRole(Long userId, String role);
    void deleteByWorkspaceId(Long workspaceId);
    int countByWorkspaceIdAndRole(Long workspaceId, String role);
}
