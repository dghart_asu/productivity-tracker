package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.Permission;

import java.util.List;

public interface PermissionService {
    List<Permission> getPermissions(Long userId);
    List<Permission> getPermissions(Long userId, Long workspaceId);
}
