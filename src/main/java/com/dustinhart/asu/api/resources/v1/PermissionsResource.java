package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.models.Permission;
import com.dustinhart.asu.api.resources.v1.dtos.PermissionDto;
import com.dustinhart.asu.api.services.PermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/ws/v1/permissions")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class PermissionsResource {

    private final PermissionService permissionService;
    private final LoggedInUserService auth;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getPermissions() {
        List<Permission> permissions = permissionService
                .getPermissions(auth.getPrincipalUser().getId());
        return ResponseEntity.ok(new PermissionDto(permissions));
    }
}
