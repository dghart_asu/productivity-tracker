package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.ReportException;
import com.dustinhart.asu.api.exceptions.WorkspaceException;
import com.dustinhart.asu.api.models.*;
import com.dustinhart.asu.api.repos.ReportRepo;
import com.dustinhart.asu.api.resources.v1.dtos.CreateReportRequest;
import com.dustinhart.asu.api.resources.v1.dtos.ErrorResponse;
import com.dustinhart.asu.api.services.ReportService;
import com.dustinhart.asu.api.services.WorkspaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ws/v1/reports")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class ReportResource {

    private final ReportService reportService;
    private final WorkspaceService workspaceService;
    private final LoggedInUserService auth;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getReport(@PathVariable("id") String id) {
        Report report = reportService.getReport(Long.parseLong(id));
        return ResponseEntity.ok(report);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getReports(@RequestParam(value = "workspace", required = false) Long workspaceId) {
        User user = auth.getPrincipalUser();
        Long userId = user.getId();
        String email = user.getEmail();

        List<Report> reports;
        if (workspaceId != null) {
            if (!workspaceService.belongsToWorkspace(userId, workspaceId)) {
                throw new WorkspaceException("User does not belong to this workspace", 401);
            }
            reports = reportService.getReports(email, workspaceId);
        } else {
            reports = reportService.getReports(email);
        }

        return ResponseEntity.ok(reports);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createReport(@RequestBody CreateReportRequest request) {
        ModelMapper modelMapper = new ModelMapper();
        Report report = modelMapper.map(request, Report.class);
        report.setId(null);
        report.setUser(auth.getPrincipalUser());

        if (!workspaceService.belongsToWorkspace(report.getUser().getId(), request.getWorkspaceId())) {
            throw new WorkspaceException("User does not belong to this workspace", 401);
        }

        if(request.isEmpty()) {
            throw new ReportException("The report must have some data.");
        }

        return ResponseEntity.ok(reportService.createReport(report));
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateReport(@RequestBody CreateReportRequest request) {
        ModelMapper modelMapper = new ModelMapper();
        Report report = modelMapper.map(request, Report.class);

        if(request.isEmpty()) {
            throw new ReportException("The report must have some data.");
        }

        return ResponseEntity.ok(reportService.updateReport(auth.getPrincipalUser(), report));
    }

    @ExceptionHandler(WorkspaceException.class)
    public ResponseEntity<?> handlException(WorkspaceException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(e.getStatus()).body(res);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handlException(Exception e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(400).body(res);
    }
}
