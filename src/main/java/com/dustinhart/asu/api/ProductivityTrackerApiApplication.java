package com.dustinhart.asu.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductivityTrackerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductivityTrackerApiApplication.class, args);
	}

}
