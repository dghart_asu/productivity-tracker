package com.dustinhart.asu.api.resources.v1.dtos;

import lombok.Data;

@Data
public class CreateSummaryReportRequest {
    private Long id;
    private Long workspaceId;
    private String date;
    private String startDate;
    private String endDate;
    private String publicNotes;
    private String privateNotes;
}
