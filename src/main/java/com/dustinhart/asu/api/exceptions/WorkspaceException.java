package com.dustinhart.asu.api.exceptions;

public class WorkspaceException extends RuntimeException {
    private final int status;

    public WorkspaceException(String message, int status) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
