package com.dustinhart.asu.api.resources.v1;

import com.dustinhart.asu.api.componets.LoggedInUserService;
import com.dustinhart.asu.api.exceptions.InvalidPasswordException;
import com.dustinhart.asu.api.models.Settings;
import com.dustinhart.asu.api.resources.v1.dtos.ErrorResponse;
import com.dustinhart.asu.api.resources.v1.dtos.SettingsResponse;
import com.dustinhart.asu.api.resources.v1.dtos.UpdateSettingsRequest;
import com.dustinhart.asu.api.services.SettingsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ws/v1/settings")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class SettingsResource {

    private final SettingsService settingsService;
    private final LoggedInUserService auth;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getSettings() {
        Settings settings = settingsService.getSettings(auth.getPrincipal().getEmail());
        SettingsResponse response = new SettingsResponse(settings.isEnableNotifications());
        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateSettings(@RequestBody UpdateSettingsRequest request) {
        Settings settings = settingsService.updateSettings(
                auth.getPrincipal().getEmail(),
                request.isNotifications(),
                request.getOldPassword(),
                request.getNewPassword());

        SettingsResponse response = new SettingsResponse(settings.isEnableNotifications());
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<?> handlException(InvalidPasswordException e) {
        log.info("Caught exception: {}", e.getMessage());
        ErrorResponse res = new ErrorResponse(e.getMessage());
        return ResponseEntity.status(400).body(res);
    }
}
