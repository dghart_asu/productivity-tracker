package com.dustinhart.asu.api.componets;

import com.dustinhart.asu.api.models.User;
import com.dustinhart.asu.api.repos.UserRepo;
import com.dustinhart.asu.api.security.UserPrincipal;
import com.dustinhart.asu.api.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

@Service
@RequestScope
@RequiredArgsConstructor
public class LoggedInUserService {

    private final UserService userService;

    public UserPrincipal getPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (UserPrincipal) authentication.getPrincipal();
    }

    public User getPrincipalUser() {
        return userService.getOrCreateUser(getPrincipal().getEmail());
    }

    public User getUser(String email) {
        email = email.toLowerCase();

        return userService.getUser(email);
    }

    public String getJwt() {
        return getPrincipal().getJwt();
    }
}
