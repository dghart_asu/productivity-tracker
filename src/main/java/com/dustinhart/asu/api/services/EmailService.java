package com.dustinhart.asu.api.services;

import com.dustinhart.asu.api.models.SummaryReport;
import com.dustinhart.asu.api.models.Team;

public interface EmailService {
    void sendActivityReminder(String email, Long workspaceId);
    void sendSummaryReportNotification(Team team, String userEmail, SummaryReport report);
}
