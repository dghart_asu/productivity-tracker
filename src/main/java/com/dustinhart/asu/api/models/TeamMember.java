package com.dustinhart.asu.api.models;

import lombok.Data;

public class TeamMember {
    private User user;
    private TeamMembership teamMembership;

    public TeamMember(User user, TeamMembership teamMembership) {
        this.user = user;
        this.teamMembership = teamMembership;
    }

    public Long getUserId() {
        return user.getId();
    }

    public String getEmail() {
        return user.getEmail();
    }

    public Long getTeamMembershipId() {
        return teamMembership.getId();
    }

    public Long getTeamId() {
        return teamMembership.getTeam().getId();
    }

    public String getRole() {
        return teamMembership.getRole();
    }
}
