package com.dustinhart.asu.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Permission {
    private PermissionType type;
    private Long resourceId;

    public enum PermissionType {
        WORKSPACE_OWNER,
        WORKSPACE_MEMBER,
        TEAM_MANAGER,
        TEAM_MEMBER
    }
}
