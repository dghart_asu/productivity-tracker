package com.dustinhart.asu.api.config;


import com.dustinhart.asu.api.clients.EmailServiceClient;
import com.dustinhart.asu.api.clients.UserServiceClient;
import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// https://www.baeldung.com/intro-to-feign
@Configuration
@RequiredArgsConstructor
public class FeignConfig {

    @Value("${feign.base-urls.users}")
    private String userServiceUrl;

    @Value("${feign.base-urls.emails}")
    private String emailServiceUrl;

    @Bean
    public UserServiceClient usersFeignClient() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(UserServiceClient.class))
                .logLevel(Logger.Level.BASIC)
                .target(UserServiceClient.class, userServiceUrl);
    }

    @Bean
    public EmailServiceClient emailFeignClient() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(EmailServiceClient.class))
                .logLevel(Logger.Level.BASIC)
                .target(EmailServiceClient.class, emailServiceUrl);
    }
}
