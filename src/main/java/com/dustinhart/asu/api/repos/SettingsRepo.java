package com.dustinhart.asu.api.repos;

import com.dustinhart.asu.api.models.Settings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingsRepo extends JpaRepository<Settings, Long> {
    Settings findByUserId(Long userId);
}
